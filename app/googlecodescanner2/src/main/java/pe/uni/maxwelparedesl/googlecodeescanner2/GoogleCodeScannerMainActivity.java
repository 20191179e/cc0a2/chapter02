package pe.uni.maxwelparedesl.googlecodeescanner2;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import com.google.mlkit.common.MlKitException;
import com.google.mlkit.vision.barcode.common.Barcode;
import com.google.mlkit.vision.codescanner.GmsBarcodeScanner;
import com.google.mlkit.vision.codescanner.GmsBarcodeScannerOptions;
import com.google.mlkit.vision.codescanner.GmsBarcodeScanning;

public class GoogleCodeScannerMainActivity extends AppCompatActivity {

    TextView textViewScanner;
    Button buttonScanner;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google_code_scanner_main);


        textViewScanner = findViewById(R.id.text_view_scanner);
        buttonScanner = findViewById(R.id.button_scanner);

        buttonScanner.setOnClickListener(v->{
            GmsBarcodeScannerOptions options = new GmsBarcodeScannerOptions.Builder()
                    .setBarcodeFormats(Barcode.FORMAT_QR_CODE).build();

            GmsBarcodeScanner scanner = GmsBarcodeScanning.getClient(this,options);

            scanner
                    .startScan()
                    .addOnSuccessListener(
                            barcode -> textViewScanner.setText(getSuccessfullMessage(barcode)))
                    .addOnCanceledListener(
                            () -> textViewScanner.setText("El usuario canceló el escaneo"))
                    .addOnFailureListener(
                            e -> textViewScanner.setText(getFailureExceptionMessage((MlKitException) e)));



        });


    }
    private String getSuccessfullMessage(Barcode barcode){

    return String.format(
            getResources().getString(R.string.barcode_result),
            barcode.getRawValue(),
            barcode.getFormat(),
            barcode.getValueType()
            );
    }

    private String getFailureExceptionMessage(MlKitException e) {

        switch (e.getErrorCode()) {

            case MlKitException.CODE_SCANNER_CANCELLED:
                return "cancelo el codigo escaneado";

            case MlKitException.UNKNOWN:
                return "error de código desconocido";

            default:
                return "falló al escanear: "+e;

        }

    }

}