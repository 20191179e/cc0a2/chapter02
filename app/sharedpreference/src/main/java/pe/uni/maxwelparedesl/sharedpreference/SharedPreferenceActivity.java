package pe.uni.maxwelparedesl.sharedpreference;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class SharedPreferenceActivity extends AppCompatActivity {
    EditText editTextName, editTextMessage;
    Button button;
    CheckBox checkBox;
    SharedPreferences sharedPreferences;
    int counter=0;
    String name, message;
    Boolean isChecked;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shared_preference);

        editTextName = findViewById(R.id.edit_text_name);
        editTextMessage = findViewById(R.id.edit_text_msg);
        button = findViewById(R.id.button);
        checkBox = findViewById(R.id.check_box);

        button.setOnClickListener(v->{

            counter +=1;

            button.setText(String.valueOf(counter));

        });


    }

    @Override
    protected void onPause() {
        super.onPause();
        saveData();
    }

    private void saveData(){
        sharedPreferences = getSharedPreferences("saveData", Context.MODE_PRIVATE);
        name = editTextName.getText().toString();
        message = editTextMessage.getText().toString();
        isChecked = checkBox.isChecked();

        @SuppressLint("CommitPrefEdits") SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString("key name",name);
        editor.putString("key message",message);
        editor.putInt("key counter", counter);
        editor.putBoolean("key remember",isChecked);
        editor.apply();

        Toast.makeText(getApplicationContext(),"tus datos están guardados",Toast.LENGTH_LONG).show();
    }
}