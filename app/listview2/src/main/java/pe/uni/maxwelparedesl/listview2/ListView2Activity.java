package pe.uni.maxwelparedesl.listview2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class ListView2Activity extends AppCompatActivity {
    ListView listView;
    String[] candidatos;

    ArrayAdapter<String> arrayAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view2);

        listView = findViewById(R.id.list_view);

        candidatos = getResources().getStringArray(R.array.candidatos);

        arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,candidatos);

        listView.setAdapter(arrayAdapter);

        listView.setOnItemClickListener((parent, view, position, id) -> {
            String candidato = parent.getItemAtPosition(position).toString();
            Toast.makeText(this,String.format(getResources().getString(R.string.toast_msg),candidato,position+1),Toast.LENGTH_SHORT).show();
        });



    }
}