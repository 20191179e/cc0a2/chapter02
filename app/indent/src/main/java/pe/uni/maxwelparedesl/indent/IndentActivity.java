package pe.uni.maxwelparedesl.indent;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

public class IndentActivity extends AppCompatActivity {

    Button button;

    EditText editText,editNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_indent);

        button = findViewById(R.id.button);

        editNumber = findViewById(R.id.edit_number);

        editText = findViewById(R.id.edit_text);

        button.setOnClickListener( v->{

            String sText = editText.getText().toString();

            String sNumber =  editNumber.getText().toString() ;

            Intent intent = new Intent(IndentActivity.this,SecondActivity.class);

            intent.putExtra("TEXT",sText);

            if(!sNumber.equals("")) intent.putExtra("NUMBER", Integer.parseInt(sNumber));

            startActivity(intent);

            //finish();

        });

    }
}