package pe.uni.maxwelparedesl.game;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;


import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

public class GameActivity extends AppCompatActivity {

    TextView textViewLastAttempt, textViewRemainAttempts, textViewHint;

    EditText editTextGuessNumber;

    Button buttonFindGuessNumber;

    Boolean twoDigits, threeDigits, fourDigits;

    Random r = new Random();

    int random;

    int remainAttempts = 10;

    ArrayList<Integer> guessesList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        textViewLastAttempt = findViewById(R.id.text_view_last_attempt);

        textViewRemainAttempts = findViewById(R.id.text_view_remain_attempts);

        textViewHint = findViewById(R.id.text_view_hint);

        editTextGuessNumber = findViewById(R.id.edit_text_guess_numb);

        buttonFindGuessNumber = findViewById(R.id.button_find_guess_number);

        Intent intent = getIntent();

        twoDigits = intent.getBooleanExtra("TWO",false);

        threeDigits = intent.getBooleanExtra("THREE",false);

        fourDigits = intent.getBooleanExtra("FOUR",false);

        if(twoDigits){
            random = r.nextInt(90) + 10;
        }
        if(threeDigits){
            random = r.nextInt(900) + 100;
        }
        if(fourDigits){
            random = r.nextInt(9000) + 1000;
        }

        buttonFindGuessNumber.setOnClickListener(v->{

            String guess =  editTextGuessNumber.getText().toString() ;

            if(guess.equals("")) {
                Toast.makeText(GameActivity.this,R.string.edit_text_smg,Toast.LENGTH_LONG).show();
                return;
            }

            int num = Integer.parseInt(guess);

            guessesList.add(num);

            textViewLastAttempt.setVisibility(View.VISIBLE);

            textViewRemainAttempts.setVisibility(View.VISIBLE);

            textViewHint.setVisibility(View.VISIBLE);

            Resources res = getResources();

            remainAttempts--;

            textViewLastAttempt.setText(String.format(res.getString(R.string.text_view_last_attempt),guess));

            textViewRemainAttempts.setText(String.format(res.getString(R.string.text_view_remain_attempt),remainAttempts));

            if( num>random ){

                textViewHint.setText(R.string.edit_text_hint_menor);

            }
            if (num < random) {

                textViewHint.setText(R.string.edit_text_hint_mayor);

            }

//lose
            if( remainAttempts == 0 ){


                AlertDialog.Builder builder = new AlertDialog.Builder(GameActivity.this);

                builder.
                        setTitle(R.string.title_dialog).
                        setCancelable(false).
                        setMessage(String.format(res.getString(R.string.game_smg_lose),random,guessesList)).
                        setPositiveButton("SI",(d,w)->{

                            Intent newIntent = new Intent(GameActivity.this,MainActivity.class);

                            startActivity(newIntent);

                        }).setNegativeButton("NO",(d,w)->{

                    moveTaskToBack(true);

                    android.os.Process.killProcess(android.os.Process.myPid());

                    System.exit(1);

                }).create().show();
            }

 //win
            if( num == random ){

                AlertDialog.Builder builder = new AlertDialog.Builder(GameActivity.this);

                builder.
                        setTitle(R.string.title_dialog).
                        setCancelable(false).
                        setMessage(String.format(res.getString(R.string.game_smg_win),random,10-remainAttempts,guessesList)).
                        setPositiveButton("SI",(d,w)->{

                            Intent newIntent = new Intent(GameActivity.this,MainActivity.class);

                            startActivity(newIntent);

                        }).setNegativeButton("NO",(d,w)->{

                    moveTaskToBack(true);

                    android.os.Process.killProcess(android.os.Process.myPid());

                    System.exit(1);

                }).create().show();

            }
            editTextGuessNumber.setText("");
        });
    }
}