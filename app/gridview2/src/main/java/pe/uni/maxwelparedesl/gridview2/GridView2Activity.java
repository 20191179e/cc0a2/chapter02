package pe.uni.maxwelparedesl.gridview2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.GridView;
import android.widget.Toast;

import java.util.ArrayList;

public class GridView2Activity extends AppCompatActivity {

    GridView gridView;
    ArrayList<String> text = new ArrayList<>();
    ArrayList<Integer> image = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid_view2);

        gridView = findViewById(R.id.grid_view);

        fillArray();

        GridAdapter gridAdapter = new GridAdapter(this,text,image);

        gridView.setAdapter(gridAdapter);

        Toast.makeText(this, "El numero de columnas es "+ gridView.getNumColumns(), Toast.LENGTH_SHORT).show();
        gridView.setOnItemClickListener((parent, view, position, id) -> Toast.makeText(GridView2Activity.this ,text.get(position),Toast.LENGTH_SHORT).show());

    }

    private void fillArray(){
        text.add("Bird");
        text.add("Cat");
        text.add("Chicken");
        text.add("Fish");
        text.add("Lion");
        text.add("Monkey");
        text.add("Rabbit");
        text.add("Sheep");
        text.add("Dog");

        image.add(R.drawable.ic_launcher_foreground);
        image.add(R.drawable.ic_launcher_foreground);
        image.add(R.drawable.ic_launcher_foreground);
        image.add(R.drawable.ic_launcher_foreground);
        image.add(R.drawable.ic_launcher_foreground);
        image.add(R.drawable.ic_launcher_foreground);
        image.add(R.drawable.ic_launcher_foreground);
        image.add(R.drawable.ic_launcher_foreground);
        image.add(R.drawable.ic_launcher_foreground);

    }


}