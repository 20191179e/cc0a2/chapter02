package pe.uni.maxwelparedesl.sharedpreference2;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class SharedPreference2Activity extends AppCompatActivity {
    EditText editTextName, editTextMessage;
    Button button;
    CheckBox checkBox;
    int counter = 0 ;
    SharedPreferences sharedPreferences;
    String name, message;
    boolean isChecked;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shared_preference2);

        editTextName = findViewById(R.id.edit_text_name);
        editTextMessage = findViewById(R.id.edit_text_message);
        button = findViewById(R.id.button);
        checkBox = findViewById(R.id.checkbox);

        button.setOnClickListener(v->{
            counter++;
            button.setText(String.valueOf(counter));
        });

        retrieveData();

    }

    @Override
    protected void onPause() {
        super.onPause();
        saveData();
    }

    private void saveData() {

        sharedPreferences = getSharedPreferences("saveData", Context.MODE_PRIVATE);
        name = editTextName.getText().toString();
        message = editTextMessage.getText().toString();
        isChecked = checkBox.isChecked();

        @SuppressLint("CommitPrefEdits") SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("name", name);
        editor.putString("message", message);
        editor.putInt("counter", counter);
        editor.putBoolean("key remember", isChecked);
        editor.apply();

        Toast.makeText(SharedPreference2Activity.this,"tus datos han sido guardados",Toast.LENGTH_SHORT).show();

    }

    private void retrieveData() {
        sharedPreferences = getSharedPreferences("saveData", Context.MODE_PRIVATE);
        name = sharedPreferences.getString("name",null);
        message = sharedPreferences.getString("message",null);
        counter = sharedPreferences.getInt("counter",counter);
        isChecked = sharedPreferences.getBoolean("key remember",false);

        editTextName.setText(name);
        editTextMessage.setText(message);
        button.setText(String.valueOf(counter));
        checkBox.setChecked(isChecked);

    }

}